angular.module('starter.services', [])

.factory('Contatos', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var contatos = [];

  return {
    all: function() {
      return contatos;
    },
    add: function(contato){
      contatos.push(contato);
      console.info(contatos);
    },
    remove: function(contato) {
      contatos.splice(contatos.indexOf(contato), 1);
    },
    get: function(contatoIdx) {
      return contatos[contatoIdx];
    },
    update: function(contato, idx){
      contatos[idx] = contato;
    }
  };
});
