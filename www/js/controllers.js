angular.module('starter.controllers', [])

.controller('ContatoCtrl', function($scope, $state, Contatos) {
  $scope.titulo = "Contatos";
  $scope.contatos = Contatos.all();
  
  $scope.editar = function(idx){
    $state.go("tab.contato-edit", {contatoId: idx});
  }
})

.controller('AddCtrl', function($scope, Contatos, $state) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  $scope.titulo = "Add Contato";
  $scope.$on('$ionicView.enter', function(e) {
    
  });

  $scope.contato = {nome:"", telefone:""};
  $scope.salvar = function(){
    var contato = angular.copy($scope.contato);
    Contatos.add(contato);
    $state.go('tab.contatos');
  }
})

.controller('ContatoEditCtrl', function($scope, $stateParams, $state, Contatos) {

  $scope.titulo = "Editar Contato";
  var id = $stateParams.contatoId;
  $scope.contato = Contatos.get(id);

  $scope.salvar = function(){
    var contato = angular.copy($scope.contato);
    Contatos.update(contato, id);
    $state.go('tab.contatos');
  }

  $scope.$on("$ionicView.beforeEnter", function(event, data){
     // handle event
     console.log("BEFORE ENTER = State Params: ", data.stateParams);
  });

  $scope.$on("$ionicView.enter", function(event, data){
     // handle event
     console.log("ENTER =  State Params: ", data.stateParams);
  });

  $scope.$on("$ionicView.afterEnter", function(event, data){
     // handle event
     console.log("AFTER ENTER = State Params: ", data.stateParams);
  });

  $scope.$on("$ionicView.leave", function(event, data){
     // handle event
     console.log("AFTER ENTER = State Params: ", data.stateParams);
  });

});
